module.exports = {
  // publicPath: process.env.NODE_ENV === 'production' ? '/portfolio/' : '/',
  css: {
    loaderOptions: {
      sass: {
        prependData: `
            @import "@/styles/_variables.scss";
        `,
      },
    },
  },
  chainWebpack: config => {
    config.output.chunkFilename(
      `js/[name].[id].[chunkhash:8]-${new Date().toISOString()}.js`
    );
  },
};
