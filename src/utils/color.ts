export default class ColorUtil {
  private static colorTechno = new Map<string, string>([
    ['javascript', '17608a'],
    ['typescript', '146acc'],
    ['node-js', '178484'],
    ['express', '207171'],
    ['vue-js', '2c3e50'],
    ['angular', '073d70'],
    ['react', '1d456b'],
    ['electron', '097e95'],
    ['mocha', '2f5469'],
    ['karma', '244d65'],
    ['jasmine', '2f5469'],
    ['chart-js', '326b8c'],
    ['d3-js', '204a7d'],
    ['html', 'bb4327'],
    ['css', '9528a2'],
    ['scss', 'bf4080'],
    ['ionic', '3567bf'],
    ['cordova', '296d80'],
    ['docker', '0973a5'],
    ['java', '40672b'],
    ['java-spring', '5c6b59'],
    ['junit', '4c6b3b'],
    ['arduino', '19442c'],
    ['neo4j', '3c7759'],
    ['sql', '16795e'],
    ['mysql', '356f5f'],
    ['postgresql', '0b634b'],
    ['couchdb', '205d3e'],
    ['mangodb', '22674d'],
    ['php', '56369c'],
    ['codeigniter', '522ca5'],
    ['lumen', '6d42ca'],
    ['devops', '9c1e1e'],
    ['gitlab-ci', 'b5331e'],
    ['github-actions', '803226'],
    ['npm', '942424'],
    ['jenkins', 'b92716'],
    ['unit-test', '8b392c'],
    ['spotify', '0d6739'],
    ['shell', '1c522b'],
    ['linux', '165026'],
    ['bash', '0d6725'],
    ['terminal', '135a06'],
  ]);

  static getColor(techno: string): string | undefined {
    return this.getColorFormat(this.formatTechno(techno));
  }

  static formatTechno(techno: string): string {
    if (!techno) {
      return '';
    }
    return techno
      .toLowerCase()
      .split('.')
      .join('-')
      .split(' ')
      .join('-');
  }

  static getColorFormat(technoFormat: string): string | undefined {
    return this.colorTechno.get(technoFormat);
  }
}
