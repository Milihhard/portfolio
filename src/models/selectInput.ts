export interface SelectInput {
  text: string;
  value: string;
  color?: string;
}
