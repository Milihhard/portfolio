import {Yaml} from 'yaml-validator-typescript';

export interface CV {
  information: Information;
  projects: Project[];
  experiences: Experiences;
}

export interface Experiences {
  professional: ProfessionalExp[];
}

export interface Information {
  desc: string;
  subtitle: string;
}

export interface Project {
  name: string;
  desc: string;
  tags: string[];
  logo: string;
  gif: string;
  images: string[];
  links: ProjectLink[];
}

export type ProjectLinkType = 'gitlab' | 'web' | 'github' | 'npm';

export interface ProjectLink {
  type: ProjectLinkType;
  url: string;
}

export interface ProfessionalExp {
  company: string;
  date: {
    short: string;
    long: string;
  };
  logo: string;
  address: string;
  status: Status[];
}

export interface Status {
  name: string;
  projects: CompanyProject[];
}

export interface CompanyProject {
  name: string;
  tasks: string[];
  tags: string[];
}

class CompanyProjectImp implements CompanyProject, Yaml {
  keysToTest: (keyof CompanyProjectImp)[] = ['name', 'tasks', 'tags'];
  optionals: Set<string> = new Set(['tasks', 'tags']);
  name = '';
  tasks: string[] = [''];
  tags: string[] = [''];
}
class ProfessionalImp implements ProfessionalExp {
  logo = '';
  address = '';
  company = '';
  date = {long: '', short: ''};
  status = [{name: '', projects: [new CompanyProjectImp()]}];
}

class ProjectImp implements Project, Yaml {
  keysToTest: (keyof ProjectImp)[] = [
    'name',
    'desc',
    'tags',
    'logo',
    'gif',
    'images',
    'links',
  ];
  optionals?: Set<string> = new Set(['logo', 'gif', 'images', 'links']);
  name = '';
  desc = '';
  tags: string[] = [''];
  logo = '';
  gif = '';
  images: string[] = [''];
  links: ProjectLink[] = [{type: 'web', url: ''}];
}

export class CVImp implements CV {
  information: Information = {desc: '', subtitle: ''};
  projects: Project[] = [new ProjectImp()];
  experiences: Experiences = {
    professional: [new ProfessionalImp()],
  };
}
