import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import yaml from 'js-yaml';
import store from '.';
import {CV, Project, Information, ProfessionalExp} from '@/models/cv';

@Module({dynamic: true, store, name: 'player'})
class CvModule extends VuexModule {
  public cv: CV | null = null;
  public selectedProject: Project | null = null;

  get projects(): Project[] {
    return this.cv ? this.cv.projects : [];
  }

  get information(): Information {
    return this.cv ? this.cv.information : {desc: '', subtitle: ''};
  }

  get professionalExp(): ProfessionalExp[] {
    return this.cv ? this.cv.experiences.professional : [];
  }

  @Mutation
  public setCV(cv: CV) {
    this.cv = cv;
  }

  @Mutation
  public setSelectedProject(project: Project) {
    this.selectedProject = project;
  }

  @Mutation
  public clearSelectedProject() {
    this.selectedProject = null;
  }

  @Action
  getPortfolio(): Promise<CV> {
    return new Promise((resolve, reject) => {
      const xobj = new XMLHttpRequest();
      // eslint-disable-next-line
      console.debug('proc', process.env);
      this.context.commit('addLoading');
      xobj.open('GET', `${process.env.BASE_URL}cv.yaml`, true);
      xobj.onreadystatechange = () => {
        if (xobj.readyState == 4 && xobj.status == 200) {
          try {
            const doc = yaml.safeLoad(xobj.responseText);
            this.context.commit('setCV', doc);
            resolve(doc);
          } catch (e) {
            reject(e);
          } finally {
            this.context.commit('removeLoading');
          }
        }
      };
      xobj.send(null);
    });
  }
}

export default CvModule;
