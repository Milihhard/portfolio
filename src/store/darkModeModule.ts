import {VuexModule, Module, Mutation} from 'vuex-module-decorators';
import store from '.';

@Module({dynamic: true, store, name: 'darkMode'})
class DarkModeModule extends VuexModule {
  private darkMode = false;

  get isDarkMode(): boolean {
    return this.darkMode;
  }

  @Mutation
  public setDarkMode(isDark: boolean) {
    this.darkMode = isDark;
  }
}

export default DarkModeModule;
