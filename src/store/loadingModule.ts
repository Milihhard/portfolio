import {VuexModule, Module, Mutation} from 'vuex-module-decorators';
import store from '.';

@Module({dynamic: true, store, name: 'loading'})
class LoadingModule extends VuexModule {
  private numberOfLoad = 0;

  get isLoading(): boolean {
    return this.numberOfLoad !== 0;
  }

  @Mutation
  public addLoading() {
    this.numberOfLoad++;
  }

  @Mutation
  public removeLoading() {
    this.numberOfLoad--;
    if (this.numberOfLoad < 0) {
      this.numberOfLoad = 0;
    }
  }
}

export default LoadingModule;
